import { Component } from '@angular/core';
import { Post } from './post.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {



  postList = [
      new Post('Tutoriel de AngularJS', 'Siquis enim militarium vel honoratorum aut nobilis inter suos rumore tenus esset insimulatus fovisse partes hostiles.'),
      new Post('Limite de temps pour l apprendre :', 'Dépends du temps investis dans mes divers projets, mais normalement finissable avant la date limite. Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere.'),
      new Post('Type de mission', 'Très intéressante, projet complexe et organisé. Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere.')
  ];

  constructor() 
  {

  }
}