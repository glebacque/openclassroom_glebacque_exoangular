export class Post {
  title: String;
  content: String;
  loveIts: number;
  createdAt: Date;

  constructor(titre: String, contenu: String)
  {
    this.title = titre;
    this.content = contenu;
    this.loveIts = 0;
    this.createdAt = new Date();
  }
}